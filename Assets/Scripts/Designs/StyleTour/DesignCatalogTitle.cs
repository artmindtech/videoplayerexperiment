using TMPro;
using Tools;
using UnityEngine;
using UnityEngine.UI;

public class DesignCatalogTitle : MonoBehaviour
{
    [SerializeField] private Image _avatar;
    [SerializeField] private TextMeshProUGUI _authorName;
    [SerializeField] private TextMeshProUGUI _budget;

    private void OnValidate()
    {
        if (_avatar == null)
            _avatar = transform.GetComponentInParent<Image>();
    }

    public void Init(PreviewVideoData data)
    {
        App.Instance.TextureLoader.Get(data.AvatarUrl,
            texture2D =>
            {
                _avatar.sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height),
                    Vector2.zero);
            }, e => { Debug.LogWarning($"SimpleVideoPreview <color=cyan>Init</color> No Avatar Image"); });
        _authorName.text = data.Name;
        _budget.text = StringUtils.ReplaceLocalized("Designs.Budget", 2.ToString());
    }
}