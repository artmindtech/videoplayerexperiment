﻿using UnityEngine;
using UnityEngine.UI;

public class RoomTourResizer : MonoBehaviour
{
    [SerializeField] private RectTransform _rectTransform;
    [SerializeField] private LayoutElement _titleLayout;
    [SerializeField] private LayoutElement _videoLayout;
    [SerializeField] private LayoutElement _catalogLayout;
    [SerializeField] private VideoPlayerUI _videoPlayer;

    private void OnValidate()
    {
        if (_videoPlayer == null)
        {
            _videoPlayer = transform.GetComponentInChildren<VideoPlayerUI>();
        }

        if (_rectTransform == null)
        {
            _rectTransform = transform as RectTransform;
        }
    }

    private void Awake()
    {
        _videoPlayer.OnResize += OnResize;
    }

    private void OnResize()
    {
        var ratio = _videoPlayer.GetRatio();
        var rect = _rectTransform.rect;
        if ((rect.width - _videoPlayer.GetBorderOffset()) / rect.height < ratio)
        {
            _titleLayout.minWidth = rect.width;
            _videoLayout.minWidth = rect.width;
            _catalogLayout.minWidth = rect.width;
            _videoLayout.minHeight = (rect.width - _videoPlayer.GetBorderOffset()) / ratio;
        }
        else
        {
            var width = rect.height * ratio;
            _titleLayout.minWidth = width;
            _videoLayout.minWidth = width;
            _catalogLayout.minWidth = width;
            _videoLayout.minHeight = rect.height;
        }
    }

    public void Resize()
    {
        OnResize();
    }
}