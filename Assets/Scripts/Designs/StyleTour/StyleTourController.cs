﻿using DG.Tweening;
using Tools.UI.Helps;
using UnityEngine;
using UnityEngine.UI;

public class StyleTourController : MonoBehaviour
{
    [SerializeField] private DesignCatalogTitle _title;
    [SerializeField] private VideoPlayerUI _videoPlayer;
    [SerializeField] private GameObject _catalogByRooms;
    [SerializeField] private RoomTourResizer _roomTourResizer;
    [SerializeField] private Button _back;
    [SerializeField] private Image _background;
    [SerializeField] private Color _portraitColor = Color.white;
    [SerializeField] private Color _landscapeColor = Color.black;
    [SerializeField] private float _changeColorSpeed = 0.3f;
    [Header("External")]
    [SerializeField] private HeaderUI _header;
    [SerializeField] private StylesCatalog _stylesCatalog;
    [SerializeField] private DesignsController _designStyleController;
    [SerializeField] private Button _applyButton;
    private bool _init;
    private OrientationRect _orientation;
    private VerticalLayoutGroup _verticalGroup;

    private void Awake()
    {
        gameObject.SetActive(_init);
        _orientation = _roomTourResizer.GetComponent<OrientationRect>();
        _verticalGroup = _roomTourResizer.GetComponent<VerticalLayoutGroup>();
        _orientation.OnChangeOrientation += OnChangeOrientation;
        // ShowTour(new PreviewVideoData{VideoUrl = "https://storage.yandexcloud.net/design-styles-catalog/Polyarnaya_25/Videos/test_full_fps25_crf30_x264.mp4"});
        _back.onClick.AddListener(OnBack);
        _applyButton.onClick.AddListener(OnApply);
    }

    private void OnApply()
    {
        _designStyleController.ApplyDesign();
        // _header.ExecuteBack();
    }

    private void OnBack()
    {
        _header.ExecuteBack();
    }

    private void OnChangeOrientation()
    {
        if (_orientation.PortraitOrientation)
        {
            _verticalGroup.padding = new RectOffset(0, 0, _header.GetHeight(), 0);
            _catalogByRooms.SetActive(true);
            _back.gameObject.SetActive(false);
            _header.Show();
            _background.DOColor(_portraitColor, _changeColorSpeed);
        }
        else
        {
            _verticalGroup.padding = new RectOffset(0, 0, 0, 0);
            _back.gameObject.SetActive(true);
            _header.Hide();
            _background.DOColor(_landscapeColor, _changeColorSpeed);
        }
        
        _videoPlayer.SetPortraitOrientation(_orientation.PortraitOrientation);
        _catalogByRooms.SetActive(_orientation.PortraitOrientation);
        _title.gameObject.SetActive(_orientation.PortraitOrientation);
        _applyButton.gameObject.SetActive(_orientation.PortraitOrientation);
        _roomTourResizer.Resize();
    }

    public void ShowTour(PreviewVideoData data)
    {
        _init = true;
        _header.Init("Design Tour", OnReturnCatalog);
        Show();
        _title.Init(data);
        _videoPlayer.Init(data.VideoUrl);
        OnChangeOrientation();
    }

    private void OnReturnCatalog()
    {
        _stylesCatalog.ComeBack();
        gameObject.SetActive(false);
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }
}