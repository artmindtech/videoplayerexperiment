﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PlayerUI : MonoBehaviour
{
    private VideoPlayer _videoPlayer;

    [SerializeField] private bool AutoPlay;
    [SerializeField] private Button PlayButton;
    [SerializeField] private Text PlayLabel;
    [SerializeField] private Slider Progress;
    [SerializeField] private Text Frames;
    private bool _checkSlider;
    private bool _seekPlay;

    private void Start()
    {
        PlayButton.onClick.AddListener(OnClick);
        PlayLabel.text = "Play";
        Progress.interactable = false;
        Progress.onValueChanged.AddListener(OnChangePlayback);
    }

    private void OnChangePlayback(float value)
    {
        if (_checkSlider)
        {
            Debug.LogWarning($"PlayerUI <color=cyan>OnChangePlayback</color> {value}");
        }
    }

    private void OnClick()
    {
        Debug.LogWarning($"PlayerUI <color=cyan>OnClick</color> _videoPlayer.isPrepared:{_videoPlayer.isPrepared} {_videoPlayer.isPlaying}");
        if (!_videoPlayer.isPrepared)
        {
            _videoPlayer.Prepare();
            PlayButton.interactable = false;
            Debug.LogWarning($"PlayerUI <color=cyan>OnClick Prepare</color>");
            return;
        }
        if (_videoPlayer.isPlaying)
            _videoPlayer.Pause();
        else
            _videoPlayer.Play();
    }

    void Update()
    {
        Frames.text = $"Frames: {_videoPlayer.frame}";
        if (_videoPlayer == null || !_videoPlayer.isPrepared)
            return;
        if (!_videoPlayer.isPrepared)
        {
            PlayLabel.text = "Wait";
        }
        else
        {
            if (_videoPlayer.isPlaying)
            {
                PlayLabel.text = "Pause";
            }
            else
            {
                PlayLabel.text = "Play";
            }
        }
        if (Input.touchCount != 0 || Input.GetMouseButton(0))
        {
            _checkSlider = true;
        }
        else
        {
            if (_checkSlider)
            {
                _checkSlider = false;
                if (_videoPlayer.isPlaying)
                {
                    _seekPlay = true;
                    _videoPlayer.Pause();
                }
                else
                {
                    _seekPlay = false;
                }
                _videoPlayer.frame = (int)(_videoPlayer.frameCount * Progress.value);
                Progress.interactable = false;
            }
            else
            {
                if(Progress.interactable)
                    Progress.value = _videoPlayer.frame / (float)_videoPlayer.frameCount;
            }
        }
    }

    public void Init(VideoPlayer videoPlayer)
    {
        _videoPlayer = videoPlayer;

        // Each time we reach the end, we slow down the playback by a factor of 10.
        _videoPlayer.loopPointReached += OnEndReached;
        _videoPlayer.prepareCompleted += OnPrepareCompleted;
        _videoPlayer.seekCompleted += OnSeekComplete;
        if (AutoPlay)
        {
            _videoPlayer.Play();
        }
    }

    private void OnSeekComplete(VideoPlayer source)
    {
        Debug.LogWarning($"PlayerUI <color=cyan>OnSeekComplete</color> {_videoPlayer.frame}");
        Progress.interactable = true;
        if (_seekPlay)
        {
            _videoPlayer.Play();
        }
    }

    private void OnPrepareCompleted(VideoPlayer source)
    {
        _videoPlayer.Play();
        PlayButton.interactable = true;
    }

    private void OnEndReached(VideoPlayer vp)
    {
        Debug.LogWarning($"VideoPlayerExample <color=cyan>OnEndReached</color> vp.playbackSpeed:{vp.playbackSpeed}");
        // vp.playbackSpeed = vp.playbackSpeed / 10.0F;
    }
}