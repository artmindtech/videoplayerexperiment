﻿using System;
using DG.Tweening;
using Tools;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class VideoPlayerUI : MonoBehaviour, ISeekable, IPointerDownHandler
{
    private const string PP_SaveAudioMute = "save_audio_mute";
    [SerializeField] private VideoPlayer _videoPlayer;
    [SerializeField] private RawImage _videoScreen;
    [SerializeField] private Button _play;
    [SerializeField] private Button _pause;
    [SerializeField] private Button _soundOn;
    [SerializeField] private Button _soundOff;
    [SerializeField] private SeekBar _seekBar;
    [SerializeField] private Image _seekLocker;
    [SerializeField] private Image _leftArrowSeek;
    [SerializeField] private Image _rightArrowSeek;
    [SerializeField] private GameObject _angles;
    [SerializeField] private Text _time;
    [SerializeField] [Range(0, 0.4f)] private float _seekSizePercent = 0.25f;
    [SerializeField] private float _animationTime = 0.5f;
    [SerializeField] private float _animationShortTime = 0.25f;
    [SerializeField] private bool _autoPlay;
    [SerializeField] private float _seekTime = 15f;
    
    public Action OnResize;
    
    private RectTransform _frameSize;
    private ulong _frameCount;

    private enum PlayerState
    {
        NoInit,
        Prepare,
        Stop,
        Pause,
        Play,
        SeekLeft,
        SeekRight,
    }

    private PlayerState _state = PlayerState.NoInit;
    private Sequence _seekArrowsAnimation;
    private RectTransform _rect;
    private RenderTexture _videoTexture;
    private bool _init;

    private void Awake()
    {
        _rect = GetComponent<RectTransform>();
        _seekBar.Init(this);
        _play.onClick.AddListener(OnPlay);
        _pause.onClick.AddListener(OnPause);
        _soundOn.onClick.AddListener(OnSoundOn);
        _soundOff.onClick.AddListener(OnSoundOff);
        _videoPlayer.prepareCompleted += OnPrepareComplete;
        _videoPlayer.seekCompleted += OnSeekComplete;
        _videoPlayer.errorReceived += OnError;
        _pause.interactable = _play.interactable = false;
        _time.text = "--:-- / --:--";
        SetState(PlayerState.Prepare);
        _soundOff.gameObject.SetActive(false);
        _soundOn.gameObject.SetActive(false);
        _time.gameObject.SetActive(false);
        _pause.gameObject.SetActive(false);
        _play.gameObject.SetActive(false);
        _seekLocker.gameObject.SetActive(false);
        _leftArrowSeek.gameObject.SetActive(false);
        _rightArrowSeek.gameObject.SetActive(false);
    }
    
    private void OnSoundOff()
    {
        _soundOff.interactable = false;
        for (ushort i = 0; i < _videoPlayer.audioTrackCount; i++)
            _videoPlayer.SetDirectAudioMute(i, false);
        PlayerPrefs.SetInt(PP_SaveAudioMute, 0);
        AnimationHide(_soundOff.GetComponent<Image>(), () =>
        {
            _soundOff.interactable = true;
            _soundOn.gameObject.SetActive(true);
        });
    }

    private void OnSoundOn()
    {
        _soundOn.interactable = false;
        for (ushort i = 0; i < _videoPlayer.audioTrackCount; i++)
            _videoPlayer.SetDirectAudioMute(i, true);
        PlayerPrefs.SetInt(PP_SaveAudioMute, 1);
        AnimationHide(_soundOn.GetComponent<Image>(), () =>
        {
            _soundOn.interactable = true;
            _soundOff.gameObject.SetActive(true);
        });
    }

    private void OnError(VideoPlayer source, string message)
    {
        Debug.LogError($"VideoPlayerUI <color=cyan>OnError {source.clip}</color> message:{message}");
        _videoPlayer.Stop();
    }

    private void OnSeekComplete(VideoPlayer source)
    {
        SetState(_videoPlayer.isPlaying ? PlayerState.Play : PlayerState.Pause);
    }

    private void Update()
    {
        if (_state.Equals(PlayerState.Pause) || _state.Equals(PlayerState.Play))
        {
            _seekBar.UpdateSeek();
            _time.text = $"{StringUtils.Timeout((int)_videoPlayer.time)} / {StringUtils.Timeout((int)_videoPlayer.length)}";
        }
        if (Input.deviceOrientation == DeviceOrientation.FaceDown)
        {
            if (_state.Equals(PlayerState.Play))
            { 
                SetState(PlayerState.Pause);
            }
        }
    }

    private void OnPrepareComplete(VideoPlayer source)
    {
        _pause.interactable = _play.interactable = true;
        _frameCount = _videoPlayer.frameCount;
        if (_videoTexture == null || !_videoTexture.width.Equals((int) _videoPlayer.width) || !_videoTexture.height.Equals((int) _videoPlayer.height))
        {
            _videoTexture = new RenderTexture((int) _videoPlayer.width, (int) _videoPlayer.height, 0);
            _videoScreen.texture = _videoTexture;
            _videoPlayer.targetTexture = _videoTexture;
            OnResize?.Invoke();
        }
        SetState(_autoPlay ? PlayerState.Play : PlayerState.Pause);
    }

    private void OnPause()
    {
        if (_init)
            SetState(PlayerState.Pause);
    }

    private void OnPlay()
    {
        if(_init)
            SetState(PlayerState.Play);
    }

    private void SetState(PlayerState state)
    {
        if (_state == state)
        {
            return;
        }
        ClearSeekArrowAnimation();
        _soundOff.gameObject.SetActive(_videoPlayer.GetDirectAudioMute(0));
        _soundOn.gameObject.SetActive(!_videoPlayer.GetDirectAudioMute(0));
        _pause.gameObject.SetActive(false);
        _time.gameObject.SetActive(false);
        _seekLocker.gameObject.SetActive(false);
        if(_leftArrowSeek.gameObject.activeSelf)
            AnimationHide(_leftArrowSeek);
        if(_rightArrowSeek.gameObject.activeSelf)
            AnimationHide(_rightArrowSeek);
        _state = state;
        switch (state)
        {
            case PlayerState.Prepare:
                break;
            case PlayerState.Stop:
                _videoPlayer.Stop();
                _play.gameObject.SetActive(true);
                break;
            case PlayerState.Pause:
                _videoPlayer.Pause();
                if (!_play.gameObject.activeSelf)
                {
                    AnimationHide(_pause.GetComponent<Image>(), () =>
                    {
                        _play.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        _play.transform.DOScale(Vector3.one, _animationShortTime);
                        _play.gameObject.SetActive(true);
                    });
                }
                _time.gameObject.SetActive(true);
                break;
            case PlayerState.Play:
                _videoPlayer.Play();
                if(_play.gameObject.activeSelf)
                    AnimationHide(_play.GetComponent<Image>());
                break;
            case PlayerState.SeekLeft:
                _seekLocker.gameObject.SetActive(true);
                _leftArrowSeek.gameObject.SetActive(true);
                _leftArrowSeek.color = Color.white;
                _seekArrowsAnimation = DOTween.Sequence();
                _seekArrowsAnimation.Append(_leftArrowSeek.DOFade(0, _animationTime));
                _seekArrowsAnimation.Append(_leftArrowSeek.DOFade(1, _animationShortTime)).SetLoops(-1);
                break;
            case PlayerState.SeekRight:
                _seekLocker.gameObject.SetActive(true);
                _rightArrowSeek.gameObject.SetActive(true);
                _rightArrowSeek.color = Color.white;
                _seekArrowsAnimation = DOTween.Sequence();
                _seekArrowsAnimation.Append(_rightArrowSeek.DOFade(0, _animationTime));
                _seekArrowsAnimation.Append(_rightArrowSeek.DOFade(1, _animationShortTime)).SetLoops(-1);
                break;
        }
    }

    private void ClearSeekArrowAnimation()
    {
        if (_seekArrowsAnimation != null)
        {
            _seekArrowsAnimation.Kill();
            _seekArrowsAnimation = null;
        }
    }

    private void OnDestroy()
    {
        ClearSeekArrowAnimation();
    }

    private void AnimationHide(Image view, Action actionOnFine = null)
    {
        view.gameObject.SetActive(true);
        view.color = Color.white;
        view.transform.localScale = Vector3.one / 5f;
        view.transform.DOScale(Vector3.one, _animationTime);
        view.DOFade(0, _animationTime).onComplete = () =>
        {
            view.gameObject.SetActive(false);
            view.color = Color.white;
            actionOnFine?.Invoke();
        };
    }

    public void Seek(float value)
    {
        var frameCount = (long)(_frameCount * Mathf.Clamp01(value));
        if (_videoPlayer.frame != frameCount)
        {
            SetState(frameCount > _videoPlayer.frame ? PlayerState.SeekRight : PlayerState.SeekLeft);
            _videoPlayer.frame = frameCount;
        }
    }

    public float GetSeek()
    {
        var percent = Mathf.Clamp01(_videoPlayer.frame / (float)_frameCount);
        return percent;
    }

    public void Play()
    {
        SetState(PlayerState.Play);
    }

    public void Stop()
    {
        SetState(PlayerState.Stop);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!_videoPlayer.isPrepared)
        {
            return;
        }
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_rect, eventData.position, null, out Vector2 result);
        var click = result - new Vector2(_rect.rect.x, _rect.rect.y);
        var seekStep = _seekTime / (float)_videoPlayer.length;
        if (click.x < _rect.rect.width * _seekSizePercent)
        {
            Seek(GetSeek() - seekStep);
        }
        else if (click.x > _rect.rect.width - _rect.rect.width * _seekSizePercent)
        {
            Seek(GetSeek() + seekStep);
        }
        else
        {
            SetState(_videoPlayer.isPlaying ? PlayerState.Pause : PlayerState.Play);
        }
    }

    public void Init(string url)
    {
        _videoPlayer.url = url;
        var muteAudio = PlayerPrefs.GetInt(PP_SaveAudioMute, 0) == 1;
        for (ushort i = 0; i < _videoPlayer.audioTrackCount; i++)
            _videoPlayer.SetDirectAudioMute(i, muteAudio);
        SetState(PlayerState.Prepare);
        _videoPlayer.Prepare();
        _init = true;
    }

    public float GetRatio()
    {
        if (_videoTexture == null)
        {
            return 1280f / 720f;
        }
        return _videoTexture.width / (float)_videoTexture.height;
    }

    public float GetBorderOffset()
    {
        return Mathf.Abs(_rect.offsetMax.x) + _rect.offsetMin.x;
    }

    public void SetPortraitOrientation(bool portraitOrientation)
    {
        // left
        _rect.offsetMin = new Vector2(portraitOrientation ? 13f : 0f, 0f);
        // right
        _rect.offsetMax = new Vector2(portraitOrientation ? -13f : 0f, 0f);
        _angles.SetActive(portraitOrientation);
    }
}

public interface ISeekable
{
    void Seek(float value);
    float GetSeek();
}