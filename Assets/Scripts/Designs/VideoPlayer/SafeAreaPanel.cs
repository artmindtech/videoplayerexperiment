﻿using System.Collections;
using Tools;
using UnityEngine;
using UnityEngine.UI;

public class SafeAreaPanel : SafeAreaCenterAndSize
{
    protected override void Resize(Vector2 ratio)
    {
        var leftScreenPadding = ratio.x * Screen.safeArea.x;
        var downScreenPadding = ratio.y * Screen.safeArea.y;
        var rightScreenPadding = ratio.x * (Screen.width - (Screen.safeArea.x + Screen.safeArea.width));
        var upScreenPadding = ratio.y * (Screen.height - (Screen.safeArea.y + Screen.safeArea.height));
        var rt = transform as RectTransform;
        rt.offsetMin = new Vector2(leftScreenPadding, downScreenPadding);
        rt.offsetMax = new Vector2(-rightScreenPadding, -upScreenPadding);
    }
}

public class SafeAreaCenterAndSize : MonoBehaviour
{
    private bool _portraitOrientation;
    private IEnumerator _cUpdateResolution;
    private Coroutine _coroutine;
    public Vector2 Center { get; private set; }
    public Vector2 Size { get; private set; }

    private void OnEnable()
    {
        _portraitOrientation = OrientationPortrait();
        ResizeInternal();
        StartUpdate();
    }

    private void OnDisable()
    {
        StopUpdate();
    }

    private void StartUpdate()
    {
        StopUpdate();
        _coroutine = StartCoroutine(CUpdateResolution());
    }

    private void OnDestroy()
    {
        StopUpdate();
    }

    private void StopUpdate()
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
            _coroutine = null;
        }
    }

    private IEnumerator CUpdateResolution()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            var newValue = OrientationPortrait();
            if (_portraitOrientation != newValue)
            {
                _portraitOrientation = newValue;
                ResizeInternal();
            }
        }
    }

    private void ResizeInternal()
    {
        var canvasScaler = GetComponentInParent<CanvasScaler>();
        var screenRealResolution = CanvasUtils.CalculateScreenSize(canvasScaler.referenceResolution, canvasScaler.matchWidthOrHeight);
        var ratio = new Vector2(screenRealResolution.x / Screen.width, screenRealResolution.y / Screen.height);
        Center = new Vector2(
            -ratio.x * (Screen.width / 2f - (Screen.safeArea.x + Screen.safeArea.width / 2f)),
            -ratio.y * (Screen.height / 2f - (Screen.safeArea.y + Screen.safeArea.height / 2f)));
        Size = new Vector2(
            ratio.x * Screen.safeArea.width,
            ratio.y * Screen.safeArea.height);
        Resize(ratio);
    }

    protected virtual void Resize(Vector2 vector2)
    {
    }
    
    protected bool IsPortrait()
    {
        return _portraitOrientation;
    }

    private bool OrientationPortrait()
    {
        return Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown;
    }
}