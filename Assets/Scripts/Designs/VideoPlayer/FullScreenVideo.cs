﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayer))]
public class FullScreenVideo : SafeAreaCenterAndSize
{
    [SerializeField] private RawImage _rawImage;
    [SerializeField] private VideoPlayer _player;
    [SerializeField] private VideoPlayerUI _ui;
    [SerializeField] private RectTransform _body;
    [SerializeField] private Button _back;
    private RenderTexture _rawTexture;
    private bool _autoPlay;
    private bool _show;

    private void OnValidate()
    {
        if (_player == null)
            _player = GetComponent<VideoPlayer>();
        if (_rawImage == null)
            _rawImage = GetComponentInChildren<RawImage>();
    }

    private void Awake()
    {
        if (!_show)
        {
            gameObject.SetActive(false);
        }
        if (_body != null)
        {
            _body.gameObject.SetActive(false);
        }
        _back.onClick.AddListener(OnBack);
    }

    private void Update()
    {
        if (_autoPlay)
        {
            _autoPlay = false;
            _player.Prepare();
            _player.prepareCompleted += OnPrepareComplete;
        }
    }

    public void Play(string url)
    {
        RenderTexture.active = _rawTexture;
        GL.Clear(true, true, Color.black);
        RenderTexture.active = null;
        _player.url = url;
        _show = true;
        gameObject.SetActive(true);
        _autoPlay = true;
    }

    private void OnPrepareComplete(VideoPlayer source)
    {
        _player.prepareCompleted -= OnPrepareComplete;
        _rawTexture = new RenderTexture((int)_player.width, (int)_player.height, 0);
        _rawImage.texture = _rawTexture;
        _player.targetTexture = _rawTexture;
        _ui.Play();
        ResizeInternal();
    }

    private void ResizeInternal()
    {
        if (!_player.isPrepared)
        {
            return;
        }
        var videoRatio = _player.width / (float)_player.height;
        var rectWidth = Size.x;
        var rectHeight = Size.y;
        var screenRatio = rectWidth / rectHeight;
        _rawImage.rectTransform.sizeDelta = videoRatio > screenRatio ? new Vector2(rectWidth, rectWidth / videoRatio) : new Vector2(rectHeight * videoRatio, rectHeight);
        if (_body != null)
        {
            if (IsPortrait())
            {
                var videoSize = _rawImage.rectTransform.sizeDelta;
                var videoPosition = new Vector2(Center.x, Center.y + rectHeight / 2 - videoSize.y / 2);
                _rawImage.rectTransform.anchoredPosition = videoPosition;
                _body.sizeDelta = new Vector2(videoSize.x, rectHeight - videoSize.y);
                _body.anchoredPosition = new Vector2(Center.x, Center.y + videoSize.y / 2f - videoSize.y);
                _body.gameObject.SetActive(true);
            }
            else
            {
                _body.gameObject.SetActive(false);
                _rawImage.rectTransform.anchoredPosition = Center;
            }
        }
        else
        {
            _rawImage.rectTransform.anchoredPosition = Center;
        }
        _rawImage.rectTransform.sizeDelta = videoRatio > screenRatio ? new Vector2(rectWidth, rectWidth / videoRatio) : new Vector2(rectHeight * videoRatio, rectHeight);
    }

    protected override void Resize(Vector2 ratio)
    {
        base.Resize(ratio);
        ResizeInternal();
    }

    public async void OnBack()
    {
        _ui.Stop();
        gameObject.SetActive(false);
        Screen.orientation = ScreenOrientation.Portrait;
        await Task.Delay(3000);
        Screen.orientation = ScreenOrientation.AutoRotation;
    }
}