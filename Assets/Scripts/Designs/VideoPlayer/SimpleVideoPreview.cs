﻿using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

public class SimpleVideoPreview : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private VideoPlayer _video;
    [SerializeField] private RawImage _videoView;
    [SerializeField] private Image _avatar;
    [SerializeField] private TextMeshProUGUI _authorName;
    [SerializeField] private TextMeshProUGUI _viewCount;
    private RenderTexture _viewTexture;
    private StylesCatalog _stylesCatalog;
    private PreviewVideoData _data;
    private bool _isPlaying;

    private void Start()
    {
        _viewTexture = new RenderTexture((int)_videoView.rectTransform.rect.width * 2, (int)_videoView.rectTransform.rect.height * 2, 0);
        _video.errorReceived += OnError;
        _video.targetTexture = _viewTexture;
        _video.url = _data.VideoPreviewUrl;
    }

    private void OnError(VideoPlayer source, string message)
    {
        Debug.LogError($"SimpleVideoPreview <color=cyan>OnError</color> message:{message}");
        _video.Stop();
    }

    private void OnRectTransformDimensionsChange()
    {
        CheckVideoSize();
    }

    public void Play()
    {
        _video.Play();
        if(!_video.isPrepared)
            _video.prepareCompleted += OnPrepareCompleted;
    }

    private void OnPrepareCompleted(VideoPlayer source)
    {
        _isPlaying = true;
        _video.prepareCompleted -= OnPrepareCompleted;
        _videoView.texture = _viewTexture;
        CheckVideoSize();
    }

    private void CheckVideoSize()
    {
        if (!_video.isPrepared)
            return;
        var rect = ((RectTransform)_videoView.transform).rect;
        var viewRatio = rect.width / (float)_video.height;
        var videoRatio = _video.width / (float)_video.height;
        _videoView.uvRect = videoRatio > viewRatio ?
            new Rect((videoRatio - viewRatio) / 2f, 0f, 1f + (viewRatio - videoRatio), 1f) :
            new Rect(0f, (viewRatio - videoRatio) / 2f, 1f, 1f + (videoRatio - viewRatio));
    }

    public void Pause()
    {
        _isPlaying = false;
        if(_video.isPlaying)
            _video.Pause();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _stylesCatalog.ShowTour(_data);
    }

    public void Init(StylesCatalog stylesCatalog, PreviewVideoData data)
    {
        name = $"VideoPreview: ..{data.VideoPreviewUrl.Substring(data.VideoPreviewUrl.Length - 40)}";
        _data = data;
        _stylesCatalog = stylesCatalog;
        App.Instance.TextureLoader.Get(_data.AvatarUrl, texture2D =>
        {
            _avatar.sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), Vector2.zero);
        }, e =>
        {
            Debug.LogWarning($"SimpleVideoPreview <color=cyan>Init</color> No Avatar Image");
        });
        App.Instance.TextureLoader.Get(_data.ScreenshotPreviewUrl, texture2D =>
        {
            if(!_isPlaying)
                _videoView.texture = texture2D;
        }, e =>
        {
            Debug.LogWarning($"SimpleVideoPreview <color=cyan>Init</color> No Video Preview Image");
        });
        _authorName.text = data.Name;
        _viewCount.text = $"{data.ViewsCount} {LocalizationManager.GetValue("Designs.Views")}";
    }
}
