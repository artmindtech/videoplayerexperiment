[System.Serializable]
public class PreviewVideoData
{
    public string VideoUrl;
    public string VideoPreviewUrl;
    public string ScreenshotPreviewUrl;
    public string AvatarUrl;
    public string Name;
    public int ViewsCount;
}