using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SeekBar : MonoBehaviour, IPointerUpHandler
{
    [SerializeField] private Image _progress;
    [SerializeField] private RectTransform _progressBackground;
    private ISeekable _seekable;
    private RectTransform _rectTransform;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    public void Init(ISeekable seekable)
    {
        _seekable = seekable;
        gameObject.SetActive(false);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, eventData.position, null, out Vector2 result);
        var click = result - new Vector2(_rectTransform.rect.x, _rectTransform.rect.y);
        _seekable.Seek(click.x / _rectTransform.rect.width);
    }

    public void UpdateSeek()
    {
        gameObject.SetActive(true);
        _progress.rectTransform.sizeDelta = new Vector2(_progressBackground.rect.width * _seekable.GetSeek(), 11);
    }
}