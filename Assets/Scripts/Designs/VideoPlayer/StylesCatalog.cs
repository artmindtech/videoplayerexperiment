﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StylesCatalog : MonoBehaviour
{
    [SerializeField] private HeaderUI _header;
    [SerializeField] private PreviewVideoData[] _urls;
    [SerializeField] private float _topSpace;
    [SerializeField] private RectTransform _content;
    [SerializeField] private SimpleVideoPreview _prefab;
    [Header("External")]
    [SerializeField] private DesignsController _controller;
    [SerializeField] private FadeAction _fadeAction;
    [SerializeField] private StyleTourController _styleTourController;

    private readonly List<SimpleVideoPreview> _items = new List<SimpleVideoPreview>();
    private int _playItem = -1;

    private void OnValidate()
    {
        if (_content == null)
            _content = GetComponentInParent<ContentSizeFitter>()?.transform as RectTransform;
    }

    private void Update()
    {
        var position = _content.anchoredPosition.y;
        var h = 0f;
        var topItem = 0;
        for (int i = 0; i < _items.Count; i++)
        {
            var itemHeight = (_items[i].transform as RectTransform).sizeDelta.y;
            if (h - position < itemHeight - _topSpace)
            {
                topItem = i;
            }

            h += itemHeight;
        }

        SetPlayPreview(topItem);
    }

    private void SetPlayPreview(int topItem)
    {
        if (topItem != _playItem)
        {
            _playItem = topItem;
            for (int i = 0; i < _items.Count; i++)
            {
                var simpleVideoPreview = _items[i];
                if (_playItem == i)
                {
                    simpleVideoPreview.Play();
                }
                else
                {
                    simpleVideoPreview.Pause();
                }
            }
        }
    }

    private void Start()
    {
        _header.Init("Каталог стилей", OnBack);
        foreach (var url in _urls)
        {
            var previewPlayer = Instantiate(_prefab, _content).GetComponent<SimpleVideoPreview>();
            previewPlayer.Init(this, url);
            _items.Add(previewPlayer);
        }
    }

    private void OnBack()
    {
        _controller?.Back();
    }

    public void ComeBack()
    {
        gameObject.SetActive(true);
        _header.Init("Каталог стилей", OnBack);
        _items[_playItem].Play();
    }

    public void ShowTour(PreviewVideoData data)
    {
        _fadeAction.Execute(() =>
        {
            _styleTourController.ShowTour(data);
            _items[_playItem].Pause();
        });
    }
}