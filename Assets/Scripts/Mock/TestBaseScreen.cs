﻿using System.Threading.Tasks;
using Base;
using UnityEngine;

public class TestBaseScreen : MonoBehaviour
{
    [SerializeField]
    private BaseScreen _screen;
    // Start is called before the first frame update
    private async void Start()
    {
        _screen.Show();
        await Task.Delay(3000);
        _screen.Hide();
        await Task.Delay(1000);
        _screen.Show();
        await Task.Delay(3000);
        _screen.Hide();
        await Task.Delay(1000);
        _screen.Show();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
