﻿namespace ArtMind.Net.Cache
{
    public abstract class CacheUrl
    {
        public string Url;
        public string[] Directories;
        public string Name;

        public bool IsInvalidUrl()
        {
            return Name == "noname";
        }

        public override string ToString()
        {
            return $"CacheUrl(Url:{Url} Name:{Name} Directories:{Directories})";
        }
    }
}