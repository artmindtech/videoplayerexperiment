namespace Tools
{
    public static class StringUtils
    {
        public static string Timeout(int timeout)
        {
            var min = (timeout / 60).ToString();
            if (min.Length < 2)
            {
                min = $"0{min}";
            }
            var sec = (timeout % 60).ToString();
            if (sec.Length < 2)
            {
                sec = $"0{sec}";
            }
            return $"{min}:{sec}";
        }

        public static string ReplaceLocalized(string text, string value)
        {
            return $"{text}: {value}";
        }
    }
}