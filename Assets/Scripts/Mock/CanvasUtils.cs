using UnityEngine;

namespace Tools
{
    public static class CanvasUtils
    {
        public static bool revertMoney = true;

        public static float CalculateScale(Vector2 reference, float matchWidthOrHeight)
        {
            float logWidth = Mathf.Log(Screen.width / reference.x, 2f);
            float logHeight = Mathf.Log(Screen.height / reference.y, 2f);
            float logWeightedAverage = Mathf.Lerp(logWidth, logHeight, matchWidthOrHeight);
            var scaleFactor = Mathf.Pow(2f, logWeightedAverage);
            return scaleFactor;
        }
        
        public static Vector2 CalculateScreenSize(Vector2 reference, float matchWidthOrHeight)
        {
            var scaleFactor = CalculateScale(reference, matchWidthOrHeight);
            return new Vector2(Screen.width / scaleFactor,Screen.height / scaleFactor);
        }
    }
}