﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlayerExample : MonoBehaviour
{
    public PlayerUI PlayerUI;
// Examples of VideoPlayer function
    void Start()
    {
        // Will attach a VideoPlayer to the main camera.
        GameObject camera = GameObject.Find("Main Camera");

        // VideoPlayer automatically targets the camera backplane when it is added
        // to a camera object, no need to change videoPlayer.targetCamera.
        var videoPlayer = camera.AddComponent<UnityEngine.Video.VideoPlayer>();

        // Play on awake defaults to true. Set it to false to avoid the url set
        // below to auto-start playback since we're in Start().
        videoPlayer.playOnAwake = false;

        // By default, VideoPlayers added to a camera will use the far plane.
        // Let's target the near plane instead.
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;

        // This will cause our Scene to be visible through the video being played.
        videoPlayer.targetCameraAlpha = 1f;

        // Set the video to play. URL supports local absolute or relative paths.
        // Here, using absolute. VideoPlayer cannot play url : file:////Users/novus/work/RiderProjects/VideoExperiment_Mobile3D/Assets/StreamingAssets/RoomTour1.mp4
        // var path = System.IO.Path.Combine(Application.streamingAssetsPath, "TestLena.mp4");
        // Debug.LogWarning($"VideoPlayerExample <color=cyan>Start</color> path:{path}");
        // videoPlayer.url = "file:///" + path;
        videoPlayer.url = "https://storage.yandexcloud.net/design-styles-catalog/Polyarnaya_25/Videos/RoomTour2.mp4";
        // videoPlayer.url = "/Users/novusis/movie.mov";

        // Skip the first 100 frames.
        videoPlayer.frame = 100;

        // Restart from beginning when done.
        // videoPlayer.isLooping = true;


        // Start playback. This means the VideoPlayer may have to prepare (reserve
        // resources, pre-load a few frames, etc.). To better control the delays
        // associated with this preparation one can use videoPlayer.Prepare() along with
        // its prepareCompleted event.

        PlayerUI.Init(videoPlayer);
        // videoPlayer.Play();
    }

    
}
