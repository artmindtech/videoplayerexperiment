﻿using System;
using UnityEngine;

namespace ArtMind.Net.Cache
{
    public class CacheHttpUrl : CacheUrl
    {
        public CacheHttpUrl(string url)
        {
            Url = url;
            Name = FileName(url);
            Directories = FileDirectories(url);
        }

        private string[] FileDirectories(string url)
        {
            var values = url.Split("/"[0]);
            string[] result = new string[Math.Max(0, values.Length - 4) + 1];
            result[0] = "";
            return result;
        }

        private string FileName(string url)
        {
            var values = url.Split("/"[0]);
            values = values[values.Length - 1].Split("?"[0]);
            var fileName = values[0];
            return fileName != "" ? fileName : "noname";
        }
    }
}