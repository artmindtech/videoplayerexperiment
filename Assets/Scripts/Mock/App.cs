using ArtMind;
using UnityEngine;

public class App
{
    public static App Instance;
    public readonly TextureLoader TextureLoader = new TextureLoader();
    private readonly Vector2 _screeSize;

    public App()
    {
        Instance = this;
        _screeSize = new Vector2(375, 776);
    }

    public Vector2 screenSize => _screeSize;
}