﻿using System;
using System.Collections.Generic;
using System.Linq;
using ArtMind.Net.Cache;
using UnityEngine;

namespace ArtMind
{
    public class TextureLoader
    {
        private const int ClearTimeout = 12000; //per sec 12000 = 30 min
        public const int ClearSizeLimit = 15000000; //per bytes 10000000 = 10M

        // private CacheManager _cacheManager;
        private WebManager _webManager;
        private static Dictionary<string, CacheTexture> _texture2Ds = new Dictionary<string, CacheTexture>();
        private static int _size;

        public TextureLoader()
        {
            // _cacheManager = new CacheManager();
            _webManager = new WebManager( 1);
        }

        public void Get(string url, Action<Texture2D> onComplete, Action<string> onError)
        {
            CheckUnload();
            var cashUrl = new CacheHttpUrl(url);
            if (cashUrl.IsInvalidUrl())
            {
                onError($"Error invalid url: {url}");
                return;
            }

            // if (_cacheManager.ExistFile(cashUrl))
            // {
            //     //Получаем из кэша
            //     Debug.LogWarning($"TextureLoader Get {cashUrl.Name} From Cache");
            //     CacheTexture texture;
            //     if (_texture2Ds.TryGetValue(url, out texture))
            //     {
            //         texture.Use();
            //         //Отдаем ранее загруженную текстуру
            //         onComplete(texture.Texture);
            //         return;
            //     }
            //
            //     var bytes = _cacheManager.LoadFile(cashUrl);
            //     if (bytes == null)
            //         _webManager.GetWeb(url, onComplete, onError);
            //     else
            //         onComplete(MakeTexture(url, bytes));
            // }
            // else
            // {
                //Загружаем если нет в кэши
                // Debug.LogWarning($"TextureLoader Get From Web url:{url}");
                _webManager.GetWeb(url, onComplete, onError);
            // }
        }

        private void CheckUnload()
        {
            DateTime dt = DateTime.Now;
            TimeSpan ts = DateTime.Now - dt;

            // var removes = new Queue<string>();
            // var texturesByTime = new List<CacheTexture>();
            // var size = 0;
            // foreach (var data in _texture2Ds)
            // {
            //     if (CacheManager.GetTime() - data.Value.Time > ClearTimeout)
            //     {
            //         removes.Enqueue(data.Key);
            //     }
            //     else
            //     {
            //         texturesByTime.Add(data.Value);
            //         size += data.Value.Size;
            //     }
            // }

            // if (size > ClearSizeLimit)
            // {
            //     texturesByTime.Sort((a, b) => a.Time > b.Time ? 1 : a.Time < b.Time ? -1 : 0);
            //     for (int i = 0; i < texturesByTime.Count; i++)
            //     {
            //         if (size > ClearSizeLimit)
            //         {
            //             removes.Enqueue(texturesByTime[0].Key);
            //             size -= texturesByTime[0].Size;
            //             texturesByTime.RemoveAt(i);
            //             i--;
            //         }
            //     }
            // }
            //
            // while (removes.Count > 0)
            // {
            //     var key = removes.Dequeue();
            //     Texture2D.Destroy(_texture2Ds[key].Texture);
            //     _texture2Ds.Remove(key);
            // }
            //
            // _size = size;
        }


        // public System.Threading.Tasks.Task<Texture2D> GetAsync(string url)
        // {
        //     var cashUrl = new CacheHttpUrl(url);
        //     if (cashUrl.IsInvalidUrl())
        //     {
        //         Debug.Log($"Error invalid url: {url}");
        //         return System.Threading.Tasks.Task.FromResult<Texture2D>(null);
        //     }
        //
        //     if (_cacheManager.ExistFile(cashUrl))
        //     {
        //         //Получаем из кэша
        //         Debug.LogWarning($"TextureLoader Get {cashUrl.Name} From Cache");
        //         CacheTexture texture;
        //         if (_texture2Ds.TryGetValue(url, out texture))
        //         {
        //             texture.Use();
        //             //Отдаем ранее загруженную текстуру
        //             return System.Threading.Tasks.Task.FromResult(texture.Texture);
        //         }
        //
        //         var bytes = _cacheManager.LoadFile(cashUrl);
        //         if (bytes == null)
        //             return _webManager.GetWebAsync(url);
        //         else
        //             return System.Threading.Tasks.Task.FromResult(MakeTexture(url, bytes));
        //     }
        //     else
        //     {
        //         //Загружаем если нет в кэши
        //         Debug.LogWarning($"TextureLoader Get From Web url:{url}");
        //         return _webManager.GetWebAsync(url);
        //     }
        // }

        public static Texture2D MakeTexture(string url, byte[] bytes)
        {
            var texture2D = new Texture2D(1, 1);
            texture2D.LoadImage(bytes);
            var size = bytes.Length;
            _texture2Ds[url] = new CacheTexture(url, texture2D, size);
            _size += size;
            return texture2D;
        }

        public void ClearCache()
        {
            _texture2Ds.Clear();
        }

        public KeyValuePair<string, CacheTexture>[] AllCache()
        {
            return _texture2Ds.ToArray();
        }

        public int AllSize()
        {
            return _size;
        }
    }

    public class CacheTexture
    {
        public string Key;
        public Texture2D Texture;
        public int Time;
        public int Size;

        public void Use()
        {
            // Time = CacheManager.GetTime();
        }

        public CacheTexture(string key, Texture2D texture, int size)
        {
            Key = key;
            Texture = texture;
            Size = size;
            Use();
        }
    }
}