
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine.Networking;

public static class UnityWebRequestExtension
{
    public static TaskAwaiter<UnityWebRequest> GetAwaiter(this UnityWebRequestAsyncOperation reqOp)
    {
        TaskCompletionSource<UnityWebRequest> tsc = new TaskCompletionSource<UnityWebRequest>();
        reqOp.completed += asyncOp => tsc.TrySetResult(reqOp.webRequest);
 
        if (reqOp.isDone)
            tsc.TrySetResult(reqOp.webRequest);
 
        return tsc.Task.GetAwaiter();
    }
}