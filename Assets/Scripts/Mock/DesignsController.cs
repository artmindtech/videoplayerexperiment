using System;
using UnityEngine;

[Serializable]
public class DesignsController : MonoBehaviour
{
    private App _app;

    private void Awake()
    {
        _app = new App();
    }

    public void Back()
    {
        Debug.Log("Back");
    }

    public void ApplyDesign()
    {
        Debug.Log("ApplyDesign");
    }
}