﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace ArtMind.Net.Cache
{
    public class WebManager
    {
        private Dictionary<string, List<WaitLoading>> waitLoadings = new Dictionary<string, List<WaitLoading>>();
        //private CacheManager cacheManager;
        private int availableLoadingCount;

        public WebManager(int availableLoadingCount = 1)
        {
            // this.cacheManager = cacheManager;
            this.availableLoadingCount = availableLoadingCount;
        }

        public async void GetWeb(string url, Action<Texture2D> onComplete, Action<string> onError)
        {
            List<WaitLoading> allWaits;
            if (waitLoadings.TryGetValue(url, out allWaits))
            {
                allWaits.Add(new WaitLoading {Complete = onComplete, Error = onError});
                return;
            }

            waitLoadings[url] = new List<WaitLoading> {new WaitLoading {Complete = onComplete, Error = onError}};
            if (waitLoadings.Count <= availableLoadingCount)
            {
                while (waitLoadings.Count > 0)
                {
                    var value = waitLoadings.First();
                    await Downloading(value.Key);
                    waitLoadings.Remove(value.Key);
                }
            }
        }

        public async Task<Texture2D> GetWebAsync(string url)
        {
            UnityWebRequest www = UnityWebRequest.Get(url);
            www.downloadHandler = new DownloadHandlerBuffer();
            await www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError) { 
                Debug.Log($"Error fail downloading. Url: {url}");
                return null;
            }
            else
            {
                var texture = TextureLoader.MakeTexture(url, www.downloadHandler.data);
                // Сохраниение файла на устройстве
                Debug.LogWarning($"TextureLoader GetWeb Save");
                //cacheManager.SaveHttpFile(new CacheHttpUrl(url), www.downloadHandler.data);
                return texture;
            }
        }

        private async Task Downloading(string url)
        {
            UnityWebRequest www = UnityWebRequest.Get(url);
            www.downloadHandler = new DownloadHandlerBuffer();
            await www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
                foreach (var waitLoading in waitLoadings[url])
                    waitLoading.Error($"Error fail downloading. Url: {url}");
            else
            {
                foreach (var waitLoading in waitLoadings[url])
                    waitLoading.Complete(TextureLoader.MakeTexture(url, www.downloadHandler.data));
                // Сохраниение файла на устройстве
                // Debug.LogWarning($"TextureLoader GetWeb Save");
                // cacheManager.SaveHttpFile(new CacheHttpUrl(url), www.downloadHandler.data);
            }
        }

        private class WaitLoading
        {
            public Action<Texture2D> Complete;
            public Action<string> Error;
        }
    }
}