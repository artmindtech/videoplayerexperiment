﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeaderUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _title;
    [SerializeField] public Button _back;
    [Header("External")]
    [SerializeField] public FadeAction _FadeAction;
    private bool _init;
    private Action _onBack;

    private void Awake()
    {
        if (!_init)
            gameObject.SetActive(false);
        _back.onClick.AddListener(OnClickBack);
    }

    private void OnClickBack()
    {
        if (_onBack != null)
        {
            if(_FadeAction != null)
                _FadeAction.Execute(_onBack);
            else
                _onBack.Invoke();
        }
    }

    public void Init(string titleValue, Action onBackCallback)
    {
        _init = true;
        _onBack = onBackCallback;
        _title.text = titleValue;
        gameObject.SetActive(true);
    }

    public int GetHeight()
    {
        return 75;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
    
    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void ExecuteBack()
    {
        OnClickBack();
    }
}