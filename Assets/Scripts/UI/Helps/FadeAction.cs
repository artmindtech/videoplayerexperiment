using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;

public class FadeAction : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private float _fadeTime = 0.2f;
    private TweenerCore<Color, Color, ColorOptions> _animation;

    private void OnValidate()
    {
        if (_image == null)
            _image = GetComponentInChildren<Image>();
    }

    private void Awake()
    {
        _image.gameObject.SetActive(true);
        HideAnimation();
    }

    public void Execute(Action action)
    {
        _image.color = new Color(1f, 1f, 1f, 0f);
        _image.gameObject.SetActive(true);
        _image.DOFade(1f, _fadeTime).onComplete = () =>
        {
            action?.Invoke();
            HideAnimation();
        };
    }

    private void HideAnimation()
    {
        _animation = _image.DOFade(0f, _fadeTime / 2f);
        _animation.onComplete = () => { _image.gameObject.SetActive(false); };
    }

    private void OnDestroy()
    {
        _animation.Kill();
        _animation = null;
    }

    public void SetActive(bool value)
    {
        gameObject.SetActive(value);
    }
}