using UnityEngine;

namespace Tools.UI.Helps
{
    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaRect : OrientationRect
    {
        private RectTransform _rect;

        protected override void ResizeInternal()
        {
            var screenSize = new Vector2(Screen.width, Screen.height);
            _rect = transform as RectTransform;
            _rect.offsetMax = Vector2.zero;
            _rect.offsetMin = Vector2.zero;
            _rect.anchorMin = Screen.safeArea.position / screenSize;
            _rect.anchorMax = _rect.anchorMin + Screen.safeArea.size / screenSize;
        }
    }
}