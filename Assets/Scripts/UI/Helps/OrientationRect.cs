using System;
using System.Collections;
using UnityEngine;

namespace Tools.UI.Helps
{
    [ExecuteAlways]
    [RequireComponent(typeof(RectTransform))]
    public class OrientationRect : MonoBehaviour
    {
        public Action OnChangeOrientation;
        [NonSerialized] public bool PortraitOrientation;
        private Coroutine _coroutine;

        protected void OnEnable()
        {
            PortraitOrientation = OrientationPortrait();
            ResizeInternal();
            StartUpdate();
        }

        private bool OrientationPortrait()
        {
            return Screen.orientation == ScreenOrientation.Portrait ||
                   Screen.orientation == ScreenOrientation.PortraitUpsideDown;
        }

        protected void OnDisable()
        {
            StopUpdate();
        }

        private void StartUpdate()
        {
            StopUpdate();
            _coroutine = StartCoroutine(CUpdateResolution());
        }

        protected void OnDestroy()
        {
            StopUpdate();
        }

        private void StopUpdate()
        {
            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
                _coroutine = null;
            }
        }

        private IEnumerator CUpdateResolution()
        {
            while (true)
            {
                var newValue = OrientationPortrait();
                if (PortraitOrientation != newValue)
                {
                    PortraitOrientation = newValue;
                    ResizeInternal();
                    OnChangeOrientation?.Invoke();
                }

                yield return new WaitForEndOfFrame();
            }
        }

        protected virtual void ResizeInternal()
        {
        }
    }
}