﻿using Base.Estimate;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;

namespace Base
{
    public class BaseScreen : MonoBehaviour, IScreen
    {
        enum AnimationType
        {
            Right,
            Bottom
        }

        [SerializeField] private float duration = 0.25f;
        [SerializeField] private Image modalBlack;
        [SerializeField] private AnimationType animation = AnimationType.Right;
        private Image _black;
        private TweenerCore<Vector2, Vector2, VectorOptions> tween;
        private bool show;

        public virtual void Hide()
        {
            if(!show)
                return;
            show = false;
            StopAnimation();
            SetupModalBlack();
            var t = (RectTransform) transform;
            switch (animation)
            {
                case AnimationType.Bottom:
                    tween = t.DOAnchorPosY(-App.Instance.screenSize.y, duration);
                    break;
                case AnimationType.Right:
                    tween = t.DOAnchorPosX(App.Instance.screenSize.x, duration);
                    break;
            }
            tween.onComplete = () =>
            {
                tween = null;
                gameObject.SetActive(false);
            };
            if (modalBlack)
                modalBlack.DOFade(0f, tween.Duration());
        }

        private void StopAnimation()
        {
            if (tween != null)
            {
                tween.Kill();
                tween = null;
            }
        }

        public void Show()
        {
            if(show)
                return;
            show = true;
            StopAnimation();
            SetupModalBlack();
            gameObject.SetActive(true);
            var t = (RectTransform) transform;
            switch (animation)
            {
                case AnimationType.Bottom:
                    t.anchoredPosition = new Vector2(0, -App.Instance.screenSize.y);
                    tween = t.DOAnchorPosY(0, duration);
                    break;
                case AnimationType.Right:
                    t.anchoredPosition = new Vector2(App.Instance.screenSize.x, 0f);
                    tween = t.DOAnchorPosX(0, duration);
                    break;
            }

            tween.onComplete = () =>
            {
                tween = null;
                if (modalBlack != null)
                    modalBlack.gameObject.SetActive(false);
            };
            if (modalBlack)
                modalBlack.DOFade(1f, tween.Duration());
        }

        private void SetupModalBlack()
        {
            if (modalBlack)
            {
                switch (animation)
                {
                    case AnimationType.Bottom:
                        break;
                    case AnimationType.Right:
                        ((RectTransform) modalBlack.transform).anchoredPosition = new Vector2(-App.Instance.screenSize.x, 0f);
                        modalBlack.gameObject.SetActive(true);
                        break;
                }
            }
        }
    }
}